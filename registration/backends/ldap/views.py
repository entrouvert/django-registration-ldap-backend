from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site
from django.shortcuts import redirect


from registration.backends.default.views import RegistrationView as DefaultRegistrationView
from registration.backends.default.views import ActivationView as DefaultActivationView

from registration import signals

from .models import ldap_manager
from .forms import LDAPRegistrationForm

class RegistrationView(DefaultRegistrationView):
    form_class = LDAPRegistrationForm

    def register(self, request, **cleaned_data):
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)
        user = ldap_manager.create_inactive_user(cleaned_data['username'],
                                                 cleaned_data['email'],
                                                 cleaned_data['password1'], site)

        signals.user_registered.send(sender=self.__class__,
                                     user=user,
                                     request=request)

        return user

class ActivationView(DefaultActivationView):

    def activate(self, request, activation_key):
        return ldap_manager.activate_user(activation_key)
