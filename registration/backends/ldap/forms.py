from django import forms
from django.utils.translation import ugettext_lazy as _

from registration.forms import RegistrationForm

from .models import ldap_manager

class LDAPRegistrationForm(RegistrationForm):

    def clean_username(self):
        if ldap_manager.existing_user(self.cleaned_data['username']):
            raise forms.ValidationError(_("A user with that username already exists."))
        else:
            return self.cleaned_data['username']
