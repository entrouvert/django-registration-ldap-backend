import re
import random
import hashlib
from datetime import datetime
import dateutil.parser
import logging

from django.conf import settings
from django.utils.timezone import now

from registration.models import SHA1_RE, RegistrationProfile

import ldap
import ldap.modlist

log = logging.getLogger(__name__)
registration_settings = settings.LDAP_REGISTRATION_BACKEND_SETTINGS

class LDAPUserManager(object):

    def __init__(self, uri=None, dn=None, password=None):
        """
        Example:
        uri: ldap://localhost
        dn: "cn=admin,dc=entrouvert,dc=org"
        basedn: "ou=people,dc=entrouvert,dc=org"
        """
        uri = uri or registration_settings['uri']
        login_dn = dn or registration_settings['binddn']
        login_pw = password or registration_settings['bindpw']

        self.path = registration_settings['users_dn']
        self.user_dn = registration_settings['user_dn']
        self.activation_token_field_name = registration_settings['activation_token_field']
        self.fields = registration_settings['fields']
        self.ACTIVATED = RegistrationProfile.ACTIVATED
        try:
            self.connection = ldap.initialize(uri)
            self.connection.whoami_s()
        except ldap.SERVER_DOWN:
            log.error('ldap server down')
        try:
            self.connection.simple_bind_s(login_dn, login_pw)
        except ldap.INVALID_CREDENTIALS:
            log.warning('invalid credentials')

    def create_inactive_user(self, username, email, password, site):
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        activation_token = hashlib.sha1(salt+email).hexdigest()

        user_attrs = ()

        data = {'username': str(username), 'password': str(password),
                'email': str(email), 'activation_token': activation_token,
                'activation_token_field': self.activation_token_field_name}

        for key, value in self.fields.iteritems():
            if key != 'objectClass':
                value = value.format(**data)
            user_attrs += ((key, value),)

        # data = (('givenName', str(username)),
        #         ('mail', str(email)),
        #         ('userPassword', str(password)),
        #         ('objectClass', ('authenticEmailValidation', 'cudPeople')),
        #         (self.token_field_name, activation_token),
        #         ('creationDate', datetime.now().isoformat())
        #     )
        self.connection.add_s('{0},{1}'.format(self.user_dn % str(username), self.path),
                                               user_attrs)

    def activate_user(self, activation_key):

        if SHA1_RE.search(activation_key):
            result = self.connection.search_s(self.path, ldap.SCOPE_ONELEVEL,
                                              '{0}={1}'.format(self.activation_token_field_name,
                                                           activation_key))
            if result:
                [(dn, attrs)] = result
                old_data = {self.activation_token_field_name: [attrs[self.activation_token_field_name][0].encode('utf-8')]}
                new_data = {self.activation_token_field_name: [self.ACTIVATED.encode('utf-8')]}
                return self.connection.modify_s(dn, ldap.modlist.modifyModlist(old_data, new_data))
            return False
        return False

    def existing_user(self, username):
        return self.connection.search_s(self.path, ldap.SCOPE_ONELEVEL,
                                          self.user_dn % str(username))

    def activation_key_expired(self, user_attrs):
        expiration_date = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)
        return activation_key == self.ACTIVATED or \
               (dateutil.parser.parse(user_attrs['creationDate']) + expiration_date <= now())

    def delete_expired_users(self):
        users = self.connection.search_s(self.path, ldap.SCOPE_ONELEVEL, 'sn=*')
        if users:
             [(dn, attrs)] = users
             for user_attrs in attrs:
                 if self.activation_key_expired(user):
                     self.connection.delete_s(dn)

ldap_manager = LDAPUserManager()
